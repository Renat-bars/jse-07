package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.model.Command;

public class CommandRep {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            " - display developer info"
    );
    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            " - display program version"
    );
    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            " - display list of commands"
    );
    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            " - display system information"
    );
    public static final Command EXIT = new Command(
            TerminalConst.INFO, null,
            " - close application"
    );

    public static final Command[] COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, INFO, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }
}
